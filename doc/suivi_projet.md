***Suivi projet***

**Architecture du projet**

A l'intérieur du dossier picross, il y aura: 
1. un dossier web contenant nos pages HTML.
2. un dossier style contenant la page CSS customisé au cas où.
3. un dosser JavaScript contenant les fichiers clic et vérification.

Si temps => header et footer en php.

Le HTML et le CSS seront développés sur la branche dev et seront inclus sur les deux branches JS lorsqu'on commencera ces dernières.

**Arbre grossier des fonctions**

**Organisation de l'équipe**

Création d'une branche dev où sera mis les dossiers web et style.
Création de deux branches pour le JS, une branche clic et une branche vérif.
Création de deux branches supplémentaires, une "cedric" et une "virginie".

Virginie s'occupe de la partie HTML et le CSS (ou Cédric finalement), recherche sur les clics.
Cédric prend en charge la création de la grille de base.

**Agenda**

Lundi 25/02 : création suivi_projet.md et début CSS et HTML.
Mardi 26/02 : fin CSS, début recherche grille, création des branches JS et des branches personnelles.
Mercredi 27/02 : création grille du picross.
Jeudi 28/02 : début recherche clics et ajout indices picross + adaptabilité de la grille.
Vendredi 1/03 : travail sur les clics.
