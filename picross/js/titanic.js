//On va chercher l'élément avec la référence 'grille' et on le stocke dans la letiable canvas.
let canvas = document.getElementById("grille");
//On crée la let ctx pour stocker le contexte de rendu 2D
let ctx = canvas.getContext("2d");


//letiables déterminant nombres de lignes, de colonnes, tailles des cases (hauteur et largeur).
let rowCount = 6;
let colCount = 6;
let squareWidth = canvas.width/rowCount;
let squareHeight = canvas.height/colCount;

/*On rentre nos cases dans un tableau à deux dimensions qui contient les colonnes de cases, qui à son tour, contient les lignes de cases
et qui contiendra à chaque fois un objet contenant une position x et y pour afficher chaque case à l'écran.*/
let squares = [];
for(let c=0; c < colCount; c++){
    squares[c] = [];
    for(let r=0; r < rowCount; r++){
        squares[c][r] = {x: 0, y: 0};
    }
}

let cluesTop = [0,1,5,4,0];
let cluesLeft = [1,2,2,3,2];

function drawClues() {
    let fontHeight = 30;

    for (i=0; i<5; i++) {
        ctx.font = fontHeight + "px sans-serif";
        ctx.fillStyle = "white"; 
        ctx.fillText(cluesTop[i], i*squareWidth+65, 35);
        ctx.fillText(cluesLeft[i], fontHeight/2, i*squareHeight+85);
  }
}   

function drawSquares(){
    for(let c=0; c < colCount; c++){
        for(let r=0; r < rowCount; r++){
            let squareX = c*squareWidth;
            let squareY = r*squareHeight;
            squares[c][r].x = squareX;
            squares[c][r].y = squareY;

            ctx.beginPath();
            ctx.rect(squareX, squareY, squareWidth, squareHeight);
            ctx.fillStyle = "#0D2742";
            ctx.fill();
            ctx.closePath();
            
            ctx.beginPath();
            ctx.rect(squareX, 0, squareWidth, squareHeight);
            ctx.fillStyle = "blue";
            ctx.fill();
            ctx.closePath();

            ctx.beginPath();
            ctx.rect(0, squareY, squareWidth, squareHeight-1);
            ctx.fillStyle = "blue";
            ctx.fill();
            ctx.closePath();
            
            ctx.beginPath();
            ctx.rect(squareX, squareY, squareWidth, squareHeight);
            ctx.strokeStyle = "black";
            ctx.stroke();
            ctx.clearRect(0, 0, squareWidth, squareHeight);
            ctx.closePath();       
        }
    }
}
/*Création d'un carré
ctx.beginPath();
ctx.rect(0, 0, 60, 60);
ctx.strokeStyle = "black";
ctx.stroke();
ctx.closePath();*/

//La fonction draw est exécutée en continue dans setInterval toutes les 10 ms.
function draw(){
drawSquares();
drawClues();
}


