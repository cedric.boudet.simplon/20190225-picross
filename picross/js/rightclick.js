canvas.oncontextmenu = function(){return false};

function rightClick(event) {
    var x = event.offsetX/50; //localise abscisse ici dans canvas et /42 pour avoir absc de la case
    var y = event.offsetY/50;
    var cornerX = Math.floor(x); // cornerX abscisse de la case
    var cornerY = Math.floor(y);
      
      if (cornerX == 0 || cornerY == 0) {   //si clic à lieu dans la partie des indices il ne fonctionne pas
              return;     
            } 
              else {
      ctx.fillStyle="red";
      ctx.fillRect (cornerX*50, cornerY*50, 50, 50);
              }  
  }